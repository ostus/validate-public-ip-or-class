<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class PublicIpOrClass implements Rule
{
	public function passes( $attribute, $value )
	{
		if( filter_var( $value, FILTER_VALIDATE_IP ) )
		{
			return !$this->isPrivate( $value );
		}

		if( strpos( $value, '/' ) !== false )
		{
			list( $ip, $bits ) = explode( '/', $value );

			if( filter_var( $ip, FILTER_VALIDATE_IP ) && is_numeric( $bits ) )
			{
				if( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4 ) )
				{
					return $bits >= 0 && $bits <= 32 && !$this->isPrivate($ip);

				}elseif( filter_var( $ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6 ) ){

					return $bits >= 0 && $bits <= 128 && !$this->isPrivate($ip);
				}
			}
		}

		return false;
	}

	public function message()
	{
		return 'The :attribute field must be a valid and public IPv4 or IPv6 address or class of public IP addresses.';
	}

	private function isPrivate( $ip ) : bool
	{
		return (bool) preg_match( '/^10\.|^172\.(1[6-9]|2[0-9]|3[01])\.|^192\.168\./', $ip );
	}
}